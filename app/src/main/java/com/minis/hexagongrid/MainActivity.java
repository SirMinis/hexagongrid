package com.minis.hexagongrid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private List<String> list = new ArrayList<>();
    private Random generator = new Random();
    private  HexagonalGridAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageCache  imageCache = new ImageCache();

        Bitmap bitmap;
        int i =0;
        for(Integer obj : DemoObjects.squareMap) {
            bitmap = BitmapFactory.decodeResource(this.getResources(), obj);
            imageCache.add(bitmap, String.valueOf(i));
            list.add(String.valueOf(i));
            i++;
        }

        RecyclerView recycler = (RecyclerView) findViewById(R.id.minisView);

        HexagonalGridLayoutManager manager = new HexagonalGridLayoutManager();

        recycler.setLayoutManager(manager);
        adapter = new HexagonalGridAdapter(this, imageCache, list);
        adapter.setOnItemClickListener(this);
        recycler.setAdapter(adapter);

        updateList();
    }

    private void updateList(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });

                    for(int i=0; i<7; i++) {
                        list.add(String.valueOf(generator.nextInt(14)));
                    }
                    Log.d("wow", String.valueOf(list.size()));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });

                    try {
                        Thread.sleep(15 * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, "item position: " + position, Toast.LENGTH_SHORT).show();
    }
}
