package com.minis.hexagongrid;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;


import java.util.List;

public class HexagonalGridAdapter extends RecyclerView.Adapter<HexagonalGridAdapter.AdapterHolder> {

    private final String TAG = this.getClass().getSimpleName();

    private AdapterView.OnItemClickListener mOnItemClickListener;
    private List<String> users;

    private Context context;

    private ImageCache imageCache;

    public HexagonalGridAdapter(Context context, ImageCache imageCache, List<String> users) {
        this.context = context;
        this.imageCache = imageCache;
        this.users = users;
    }

    public void addItem(int position) {
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        notifyItemRemoved(position);
    }

    @Override
    public AdapterHolder onCreateViewHolder(ViewGroup container, int viewType) {
        ImageView imageView = new ImageView(container.getContext());
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return new AdapterHolder(imageView, this);
    }

    @Override
    public void onBindViewHolder(AdapterHolder itemHolder, int position) {
        Bitmap bitmap = imageCache.getItem(users.get(position));
        itemHolder.setImage(bitmap);
        itemHolder.imageView.setImageBitmap(bitmap);

        float profileSize = 50;
        itemHolder.imageView.getLayoutParams().width = (int) UiHelper.convertDpToPx(profileSize, context);
        itemHolder.imageView.getLayoutParams().height = (int) UiHelper.convertDpToPx(profileSize, context);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    private void onItemHolderClick(AdapterHolder itemHolder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(null, itemHolder.itemView,
            itemHolder.getAdapterPosition(), itemHolder.getItemId());
        }
    }

//    private void setPicture(Bitmap bitmap, final ImageView img){
//        float profileSize = 50;
//        img.setImageBitmap(circleBitmap);
//        img.getLayoutParams().width = (int) UiHelper.convertDpToPx(profileSize, context);
//        img.getLayoutParams().height = (int) UiHelper.convertDpToPx(profileSize, context);
//
//    }

    public static class AdapterHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageView;
        private HexagonalGridAdapter mAdapter;

        public AdapterHolder(ImageView imageView, HexagonalGridAdapter adapter) {
            super(imageView);
            this.imageView = imageView;
            imageView.setOnClickListener(this);

            mAdapter = adapter;
        }

        @Override
        public void onClick(View v) {
            mAdapter.onItemHolderClick(this);
        }

        public void setImage(Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
