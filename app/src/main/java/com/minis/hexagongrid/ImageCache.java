package com.minis.hexagongrid;

import android.graphics.Bitmap;
import android.util.LruCache;

public class ImageCache {

    private final String TAG = this.getClass().getSimpleName();

    public LruCache<String, Bitmap> lruCacheBitmap;

    Bitmap circleBitmap;

    public ImageCache() {
        lruCacheBitmap = new LruCache<String, Bitmap>(10000000) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getByteCount() / 1024;
            }
        };
    }

    public void add(Bitmap bitmap, String id){
        pUserToBitmap(bitmap, id);
    }

    public Bitmap getItem(String facebookId) {
        circleBitmap = lruCacheBitmap.get(facebookId);
        return circleBitmap;
    }


    private void pUserToBitmap(Bitmap bitmap, String id){
        CircleTransform circleTransform = new CircleTransform();
        Bitmap circleBitmap = circleTransform.transform(bitmap);
        lruCacheBitmap.put(id, circleBitmap);
    }

    public int getSize() {
        return lruCacheBitmap.size();
    }
}
