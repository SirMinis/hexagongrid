package com.minis.hexagongrid;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;

public class HexagonalGridLayoutManager extends RecyclerView.LayoutManager{

    private static final String TAG = HexagonalGridLayoutManager.class.getSimpleName();

    private static final int DEFAULT_COUNT = 1;

    /* Consistent size applied to all child views */
    private int mDecoratedChildWidth;
    private int mDecoratedChildHeight;

    private int mChildWidth = 100;
    private int mChildHeight = 100;

    private int properHeightBottom;
    private int properWidthLeft;
    private int properHeightTop;
    private int properWidthRight;

    private final int RADIUS = 120;

//    private int moffsetsWidth = 100;
//    private int moffsetsHeight = 200;
    /* Used for tracking off-screen change events */
    private int mFirstChangedPosition;
    private int mChangedPositionCount;

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        if (getItemCount() == 0) {
            detachAndScrapAttachedViews(recycler);
            return;
        }

        if (getChildCount() == 0 && state.isPreLayout()) {
            //Nothing to do during prelayout when empty
            return;
        }

        if (!state.isPreLayout()) {
            mFirstChangedPosition = mChangedPositionCount = 0;
        }

        detachAndScrapAttachedViews(recycler);

        properHeightBottom =  getHeight() / 2 + mChildHeight / 2;
        properWidthLeft = getWidth() / 2 - mChildWidth / 2;
        properHeightTop =  getHeight() / 2 - mChildHeight / 2;
        properWidthRight = getWidth() / 2 + mChildWidth / 2;


        if (getChildCount() == 0) { //First or empty layout
            //Scrap measure one child
            View scrap = recycler.getViewForPosition(0);
            addView(scrap);
            measureChildWithMargins(scrap, 0, 0);

            mDecoratedChildWidth = getDecoratedMeasuredWidth(scrap);
            mDecoratedChildHeight = getDecoratedMeasuredHeight(scrap);
            layoutDecorated(scrap, properWidthLeft, properHeightTop, properWidthRight, properHeightBottom);

            addView(scrap);
//            detachAndScrapView(scrap, recycler);
            Log.d("TEST", mDecoratedChildWidth + " " + mDecoratedChildHeight);
        }


        if(getChildCount() < getItemCount()){
            setPositionOfViews(recycler);
        }

        Log.d("TEST", "child " + getChildCount() + " item " + getItemCount() + " statePreLayout " + state.isPreLayout());
        Log.d("TEST", "padding left " + getPaddingLeft() + " padding right " + getPaddingRight());
        fillGrid();

        SparseIntArray removedCache = null;
    }

    private void setPositionOfViews(RecyclerView.Recycler recycler){
        int numberOfElement = 1;
        int r = RADIUS;
        int y;
        int x;
        int layerNr;
        double angle = 0;
        int maxElements = getItemCount();

        for (layerNr = 1; numberOfElement < maxElements; layerNr++) {
            for (int elementInLayer = 0; elementInLayer < layerNr * 6 && numberOfElement < maxElements; elementInLayer++) {
                View view = recycler.getViewForPosition(numberOfElement);
//                Log.d("TEST", " left= " + getDecoratedLeft(view) + " right= " + getDecoratedRight(view) + " top= " + getDecoratedTop(view));
                addView(view);
                x = (int) Math.round(Math.cos(Math.toRadians(angle)) * r);
                y = (int) Math.round(Math.sin(Math.toRadians(angle)) * r);
//                Log.d("TEST", String.valueOf("element nr: " + numberOfElement + "- x: " + x + " y: " + y + " angle: " + angle));
                layoutDecorated(view, properWidthLeft + y ,properHeightTop - x , properWidthRight + y, properHeightBottom - x);
                addView(view);
                angle += 360 / (layerNr * 6);
                numberOfElement++;
            }
            r += RADIUS;
            angle = 0;
        }
    }

    private void fillGrid() {
        SparseArray<View> viewCache = new SparseArray<View>(getChildCount());
        if (getChildCount() != 0) {
            for (int i = 0; i < getChildCount(); i++) {
                int position = positionOfIndex(i);
                final View child = getChildAt(i);
                viewCache.put(position, child);
            }


//            Log.d("TEST", String.valueOf(viewCache));

//            for (int i=0; i < viewCache.size(); i++) {
//                detachView(viewCache.valueAt(i));
//            }

//            View view = viewCache.get(nextPosition);
//            if (view == null) {
//        /*
//         * The Recycler will give us either a newly constructed view,
//         * or a recycled view it has on-hand. In either case, the
//         * view will already be fully bound to the data by the
//         * adapter for us.
//         */
//                view = recycler.getViewForPosition(nextPosition);
//                addView(view);

            }


    }

    @Override
    public boolean canScrollVertically() {
        return true;
    }

    @Override
    public boolean canScrollHorizontally() {
        return true;
    }

    @Override
    public int scrollHorizontallyBy(int dx, RecyclerView.Recycler recycler, RecyclerView.State state) {
        offsetChildrenHorizontal(dx);
        return -dx;
    }

    @Override
    public int scrollVerticallyBy(int dy, RecyclerView.Recycler recycler, RecyclerView.State state) {
        offsetChildrenVertical(dy);
        return -dy;
    }

    private int getHorizontalSpace() {
        return getWidth() - getPaddingRight() - getPaddingLeft();
    }

    private int positionOfIndex(int childIndex) {
        int layerNr = 1 + (int)((-3+ Math.sqrt(12 * childIndex - 3))/(3));
        int numberInLayer = layerNr - 1 + childIndex;

        return numberInLayer;
    }
}
